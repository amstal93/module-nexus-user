provider "nexus" {
  url      = var.nexus_url
  username = var.nexus_admin_username
  password = var.nexus_admin_password
}
