terraform {
  required_version = ">= 1.1.7"

  required_providers {
    nexus = {
      source  = "datadrivers/nexus"
      version = ">= 1.18"
    }
  }
}
