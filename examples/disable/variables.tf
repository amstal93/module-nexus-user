variable "nexus_admin_password" {
  type      = string
  sensitive = true
  default   = null
}

variable "nexus_admin_username" {
  type    = string
  default = null
}

variable "nexus_url" {
  type    = string
  default = null
}
